//
//  String+AddText.swift
//  MyLocations
//
//  Created by Carolina Arcos on 2/6/18.
//  Copyright © 2018 Condor Labs. All rights reserved.
//

extension String {
    mutating func add(text: String?, separatedBy separator: String = "") { // Mutating modifies the value
        if let text = text {
            if !isEmpty {
                self += separator
            }
            self += text
        }
    }
}
