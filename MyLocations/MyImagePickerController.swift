//
//  MyImagePickerController.swift
//  MyLocations
//
//  Created by Carolina Arcos on 2/6/18.
//  Copyright © 2018 Condor Labs. All rights reserved.
//

import UIKit

class MyImagePickerController: UIImagePickerController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
