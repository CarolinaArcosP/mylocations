//
//  MyTabBarController.swift
//  MyLocations
//
//  Created by Carolina Arcos on 2/6/18.
//  Copyright © 2018 Condor Labs. All rights reserved.
//

import UIKit

class MyTabBarController: UITabBarController, UITabBarControllerDelegate {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var childViewControllerForStatusBarStyle: UIViewController? {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self;
    }
    
//    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//        if let vc = viewController as? UINavigationController {
//            vc.popToRootViewController(animated: true)
//        }
//    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let vc = viewController as? UINavigationController, !(vc.viewControllers.first is CurrentLocationViewController) {
            vc.popToRootViewController(animated: false)
            return true
        }
//        guard viewController is CurrentLocationViewController else {
//            viewController.navigationController?.popToRootViewController(animated: true)
//            return true
//        }
        return true
    }
    
}
